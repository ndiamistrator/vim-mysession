"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Utilities

function! s:fnamemodify(...) abort
    return substitute(call(function('fnamemodify'), a:000), '/\+$', '', '')
endfunction

function! s:PIDSTIME(pid) abort
    try
        return str2nr(substitute(
        \ readfile('/proc/'..(0+a:pid)..'/stat')[0],
        \ '\v.*\)%( \S+){19} (\d+).*', '\1', ''))
    catch
        return 0
    endtry
endfunction

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" if !exists('g:shadadir')
"     let g:shadadir = g:VimDataDir .'/myshada'
" endif
" 
" if !empty(get(g:, 'shadadir', ''))
"     call mkdir(g:shadadir, 'p')
" endif
" 
" let s:shadafile = printf('%06d.%s.shada', getpid(), s:PIDSTIME(getpid()))
" 
" function! Wshada() abort
"     if empty(get(g:, 'shadadir', '')) | return 0 | endif
"     exe 'keepjumps wshada! '..fnameescape(g:shadadir..'/'..s:shadafile)
"     return 1
" endfunction
" 
" function! Rshada() abort
"     rshada
"     if empty(get(g:, 'shadadir', '')) | return | endif
"     for shada in glob(fnameescape(g:shadadir)..'/*.shada', 0, 1)
"         exe 'rshada '..fnameescape(shada)
"     endfor
" endfunction
" 
" function! s:_shadacleanup() abort
"     if empty(get(g:, 'shadadir', '')) | return | endif
"     if !empty(&shada)
"         wshada
"         call delete(g:shadadir..'/'..s:shadafile)
"     endif
" endfunction
" 
" command! -bar Wshada call Wshada()
" command! -bar Rshada call Rshada()
" 
" augroup MyInit_Shada
"     autocmd!
"     autocmd CursorHold * Wshada
"     autocmd VimLeave * call s:_shadacleanup()
" augroup END
" 
" if !empty(&shada)
"     Rshada
" endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

if !exists('g:sessiontags') | let g:sessiontags = [] | endif

if !exists('g:SESSIONTAGS') | let g:SESSIONTAGS = ['tag1', 'tag2'] | endif

if !exists('g:sessiondir')
    let g:sessiondir = g:VimDataDir .'/sessions'
    " let g:sessiondir = g:VimDataDir .'/sessions.dev'
endif

let s:sessionsopt = {
\  'since': 30*24*3600,
\    'max': 300,
\   'skip': 0,
\ }
if !exists('g:sessionsopt') | let g:sessionsopt = copy(s:sessionsopt) | endif

let s:sessionfilter_bufname = '\v%(^$|^%(man|term)://|/?%(__doc__$|NvimTree$|Output of: |__[Tt]ag[Bb]ar__|\\\[))'
let s:sessionfilter_filetype = '\v^%(help)$'
if !exists('g:sessionfilter_bufname') | let g:sessionfilter_bufname = s:sessionfilter_bufname | endif
if !exists('g:sessionfilter_filetype') | let g:sessionfilter_filetype = s:sessionfilter_filetype | endif

function! s:sessionnew() abort
    return 'active/'..'session.'. strftime('%FT%H-%M-%S') .'.'. printf('%06d', getpid()) .'.'. string(s:PIDSTIME(getpid())) . '.vim'
endfunction

if !exists('g:sessionpath')
    let g:sessionpath = s:sessionnew()
endif

if !empty(get(g:, 'sessiondir', ''))
    call mkdir(g:sessiondir, 'p')
    call mkdir(g:sessiondir..'/active', 'p')
    call mkdir(g:sessiondir..'/quit', 'p')
    call mkdir(g:sessiondir..'/crash', 'p')
    call mkdir(g:sessiondir..'/.cache', 'p')
    call mkdir(g:sessiondir..'/.trash', 'p')
endif


""""""""""""""""""""""""""""""""""""""""

"set sessionoptions=blank,buffers,curdir,folds,globals,help,localoptions,options,tabpages,unix,winsize

if has('nvim')
    "if !exists('g:SessionOptions')
    "    let g:SessionOptions = ['buffers', 'curdir', 'folds',
    "                              \ 'slash', 'tabpages', 'unix']
    "endif
    if !exists('g:SessionAllOptions')
        let g:SessionAllOptions = [
                    \ 'blank', 'buffers', 'curdir', 'folds', 'globals', 'help',
                    \ 'localoptions', 'options', 'resize', 'sesdir', 'slash',
                    \ 'tabpages', 'unix', 'winpos', 'winsize']
    endif
else
    "if !exists('g:SessionOptions')
    "    let g:SessionOptions = ['buffers', 'curdir', 'folds',
    "                              \ 'slash', 'tabpages', 'unix']
    "endif
    if !exists('g:SessionAllOptions')
        let g:SessionAllOptions = [
                    \ 'blank', 'buffers', 'curdir', 'folds', 'globals', 'help',
                    \ 'localoptions', 'options', 'resize', 'sesdir', 'slash',
                    \ 'tabpages', 'unix', 'winpos', 'winsize']
    endif
endif

""""""""""""""""""""""""""""""""""""""""

function! s:sessionbufs() abort
    let bufs = []
    for bufinfo in getbufinfo()
        call add(bufs, {
        \      'name': bufinfo.name,
        \  'lastused': bufinfo.lastused,
        \   'changed': bufinfo.changed,
        \ 'linecount': bufinfo.linecount,
        \    'loaded': bufinfo.loaded,
        \    'listed': bufinfo.listed,
        \    'hidden': bufinfo.hidden,
        \  '_buftype': getbufvar(bufinfo.bufnr, '&buftype'),
        \ '_filetype': getbufvar(bufinfo.bufnr, '&filetype'),
        \ '_nwindows': len(get(bufinfo, 'windows', []))
        \ })
    endfor
    return bufs
endfunction

function! s:sessionextra(file) abort
    let extra = [strftime('" SessionTime %F %T')]
    for buf in s:sessionbufs()
        call add(extra, '" JSON Buffer '..json_encode(buf))
    endfor
    if !empty(get(g:, 'sessiontags', []))
        call extend(extra, ['if exists(":SessionTag")', 'SessionTag '.  join(g:sessiontags), 'endif'])
    else
        call extend(extra, ['if exists(":SessionTag")', '"SessionTag', 'endif'])
    endif
    "call extend(extra, ['set sessionoptions='..&sessionoptions])
    "call extend(extra, ['if exists(":SessionClean")', 'SessionClean', 'endif'])
    if !empty(extra)
        call add(extra, '" vim: set ft=vim :')
        call writefile(extra, a:file, 'a')
    endif
endfunction

function! s:updatetags() abort
    let tags = []
    for tag in g:sessiontags
        if index(tags, tag) == -1
            call add(tags, tag)
        endif
    endfor
    let g:sessiontags = tags
    let tags = reverse(copy(tags))
    for tag in reverse(copy(g:SESSIONTAGS))
        if index(tags, tag) == -1
            call add(tags, tag)
        endif
    endfor
    let g:SESSIONTAGS = reverse(tags)
endfunction

function! s:mksession() abort
    if empty(get(g:, 'sessiondir', '')) | return | endif
    if empty(get(g:, 'sessionpath', '')) | return | endif
    " XXX let sessionoptions_save = &sessionoptions
    "set sessionoptions=buffers,curdir,folds,globals,help,localoptions,options,tabpages,unix,winsize
    " XXX set sessionoptions=blank,buffers,curdir,folds,globals,help,localoptions,options,resize,slash,tabpages,unix,winpos,winsize
    try
        let session = g:sessiondir .'/'. g:sessionpath
        let sessioninvalid = session .'.invalid'
        call mkdir(s:fnamemodify(session, ':h'), 'p')
        exe 'mksession! '. fnameescape(sessioninvalid)
        call s:sessionextra(sessioninvalid)
        " Make sure that if something goes wrong while creating the 
        " session I don't overwrite a previously saved one.
        call rename(session .'.invalid', session)
        call s:updatetags()
        if has('nvim') && !empty(&shada)
            keepjumps wshada
        endif
    catch
        " I don't care if I am in a command window
        if empty(getcmdwintype())
            echohl ErrorMsg
            echomsg 'Cannot mksession: '. v:exception
            echohl None
        endif
    endtry
    " XXX let &sessionoptions = sessionoptions_save
endfunction

" function! s:sessioncmp(A, B) abort
"     return a:A[0] < a:B[0] ? -1 : a:A[0] > a:B[0] ? 1 : 0
" endfunction

function! s:sessionleave() abort
    if empty(get(g:, 'sessiondir', '')) | return | endif
    if empty(get(g:, 'sessionpath', '')) | return | endif
    try | call s:mksession() " TODO ?
    catch | endtry
    " move current session to quit ones
    let qui = g:sessiondir..'/quit/'
    let dest = qui..fnamemodify(g:sessionpath, ':t')
    if !empty(getftype(dest)) | return | endif
    call mkdir(qui, 'p')
    call rename(g:sessiondir..'/'..g:sessionpath, dest)
endfunction

function! s:_sessionleave() abort
    try
        " " delete old quit sessions. keep only last week and 20 even older.
        " let oldsessions = []
        " let time = localtime()
        " for session in glob(fnameescape(g:sessiondir) .'/quit/session-*.vim', 1, 1)
        "     let sessiontime = getftime(session)
        "     if time - sessiontime > 604800
        "     " older than 1 week
        "         call add(oldsessions, [sessiontime, session])
        "     endif
        " endfor
        " let session = ''
        " let oldsessions = sort(oldsessions, 's:sessioncmp')
        " unlet session
        " for session in oldsessions[:-21]
        "     call delete(session[1])
        " endfor
        call s:sessionleave()
    catch
      echohl ErrorMsg
      echo 'Error detected during sessionleave: '. v:exception
      echohl None
      call input('Press enter to quit... ')
    endtry
"    if sessionssize > 10485760
"        echo printf("\nWarning: There are %d old sessions occupying %dMB.\nYou should consider removing some...\n", sessions, sessionssize/1024/1024)
"    endif
"    while 1
"        let answer = input("Delete current session? (yes/no): ", 'yes')
"        if answer ==# 'yes'
"            call delete(g:sessiondir .'/'. g:sessionpath)
"            break
"        elseif answer ==# 'no'
"            break
"        endif
"    endwhile
endfunction

""""""""""""""""""""""""""""""""""""""""

function! SessionFilter(buf, ...) abort
    let opt = {
        \ 'sessionfilter_bufname':  get(g:, 'sessionfilter_bufname',  s:sessionfilter_bufname),
        \ 'sessionfilter_filetype': get(g:, 'sessionfilter_filetype', s:sessionfilter_filetype)
        \ }
    for _opt in a:000 | call extend(opt, _opt) | endfor
    if !empty(opt.sessionfilter_bufname) && a:buf.name =~# opt.sessionfilter_bufname | return 1 | endif
    "if !get(a:buf, 'loaded', 1) | return 1 | endif
    "if !get(a:buf, 'listed', 1) | return 1 | endif
    "if  get(a:buf, 'hidden', 0) | return 1 | endif
    if !empty(get(a:buf, 'bufnr', 0)) && bufexists(a:buf.bufnr) && fnamemodify(a:buf.name, ':t') ==# fnamemodify(bufname(a:buf.bufnr), ':t')
        if !exists('a:buf._buftype')  | let a:buf._buftype  = getbufvar(a:buf.bufnr, '&buftype')  | endif
        if !exists('a:buf._filetype') | let a:buf._filetype = getbufvar(a:buf.bufnr, '&filetype') | endif
    endif
    if index(['','acwrite', 'nowrite'], get(a:buf, '_buftype', '')) == -1 | return 1 | endif
    if !empty(opt.sessionfilter_filetype) && get(a:buf, '_filetype', '') =~# opt.sessionfilter_filetype | return 1 | endif
    return 0
endfunction

function! SessionFilterAll(...) abort
    return
    let winid = win_getid()
    let current = bufnr('')
    tabdo windo if call(function('SessionFilter'), [getbufinfo(bufnr(''))[0]]+a:000) | try | exe 'close' | catch | endtry | endif
    call win_gotoid(winid)
endfunction

""""""""""""""""""""""""""""""""""""""""

let s:mycompleter = {}
function! MyCompleter(words, suggest, ...) abort
    let new = copy(s:mycompleter)
    let new.words = a:words
    let new.suggest = a:suggest
    for a in a:000
        call extend(new, a)
    endfor
    return new
endfunction

function! s:mycompleter.complete(ArgLead, CmdLine, CursorPos) abort
    let match = matchlist(a:CmdLine[:a:CursorPos-1],
                \ '\v^\S+\s+(%([+-]\w+\s+)*)%(([+-]\w*)$)?(.*)')[1:3]
    if empty(match)
        return []
    endif
    let [opts, opt, remain] = match
    if empty(remain)
        " let savedopts = &sessionoptions
        try
            let words = type(self.words)==type({}) ? keys(self.words) : copy(self.words)
            for _opt in split(opts)
                let idx = index(words, _opt[1:])
                if _opt[0] == '+'
                    if idx == -1
                        call add(words, _opt[1:])
                    endif
                else
                    if idx > 0
                        call remove(words, idx)
                    endif
                endif
            endfor
        catch
            return []
        endtry
        let suggest = filter((type(self.suggest)==type({}) ? keys(self.suggest) : copy(self.suggest)),
                    \ '!(index(words, v:val)+1)')
                                      
        call map(words, '"-".v:val')
        call map(suggest, '"+".v:val')
        let effective = words + suggest

        let length = len(opt)
        return filter(effective, 'strcharpart(v:val, 0, length) ==# opt')
    else
        if !empty(get(self, 'failover', ''))
            return getcompletion(remain, self.failover)
        endif
    endif
endfunction

function! s:_SessionCompletions(...) abort
    return call(MyCompleter(split(&sessionoptions,','), g:SessionAllOptions, {'failover':'file'}).complete, a:000)
endfunction

function! s:_SessionTagCompletions(...) abort
    return call(MyCompleter(reverse(copy(get(g:, 'sessiontags', []))), reverse(copy(get(g:, 'SESSIONTAGS', [])))).complete, a:000)
endfunction

function! s:_SessionTag(ops, bang) abort
    try
        if !empty(a:bang) || !exists('g:sessiontags')
            let g:sessiontags = []
        endif
        let ops = split(a:ops)
        for op in ops
            if op =~# '^[+-]'
                let tag = op[1:]
            else
                let tag = op
            endif
            let idx = index(g:sessiontags, tag)
            if idx >= 0
                call remove(g:sessiontags, idx)
            endif
            if op !~# '^-'
                call add(g:sessiontags, tag)
            endif
        endfor
        call s:updatetags()
	if has('nvim') | exe 'wshada' | endif
        echo 'SessionTag '. join(g:sessiontags)
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction

function! SessionNew() abort
    call s:sessionleave() " TODO ?
    let g:sessionpath = s:sessionnew()
    call s:mksession()
    echomsg 'New session: '..g:sessionpath
endfunction

function! s:_Session(opts_with_path, bang) abort
    let m = matchlist(a:opts_with_path, '\v^(%([+-]\a+%(\s+|$))*)(.*)')
    if empty(m)
        throw 'invalid opts: '..opts_with_path
    endif
    let [opts, file] = m[1:2]
    if !empty(a:bang)
        call SessionNew()
    endif
    if empty(file)
        let file = '~/.session.vim'
    endif
    for opt in split(opts)
        if opt =~# '^-'
            exe 'set sessionoptions-='. opt[1:]
        else
            exe 'set sessionoptions+='. opt[1:]
        endif
    endfor
    call s:mksession()
endfunction

command! -bar SessionNew    call SessionNew()
command! -bang -nargs=? -complete=customlist,s:_SessionCompletions Session
            \ call s:_Session(<q-args>, '<bang>')
command! -bang -nargs=? -complete=customlist,s:_SessionCompletions SessionSave Session<bang> <args>
command! -bar -bang -nargs=? -complete=customlist,s:_SessionTagCompletions SessionTag
            \ call s:_SessionTag(<q-args>, '<bang>')
command! -bar SessionFilter call SessionFilterAll()

augroup MyInit_Session
    autocmd!
    autocmd CursorHold * call s:mksession()
    autocmd VimLeave * call s:_sessionleave()
augroup END

""""""""""""""""""""""""""""""""""""""""

function! s:movecrashedsessions(...) abort
    let sessiondir = get(a:000, 0, get(g:, 'sessiondir', ''))
    if empty(sessiondir) | return | endif
    let activedir = sessiondir..'/active'
    let crashdir = sessiondir..'/crash'
    call mkdir(activedir, 'p')
    call mkdir(crashdir, 'p')
    for session in readdir(activedir, {s->!isdirectory(s)})
        let split = split(session, '\.')
        if len(split) == 5
            let [prefix, time_, pid, ptime, suffix] = split
        elseif len(split) == 4
            let [prefix, time_, pid, suffix] = split
            let ptime = -1
        else
            continue
        endif
        if prefix != 'session' || suffix != 'vim'
            continue
        endif
        if s:PIDSTIME(pid) == ptime
            continue
        endif
        call writefile(readfile(activedir..'/'..session, 'b'), crashdir..'/'..session, 'b')
        call delete(activedir..'/'..session)
    endfor
endfunction

function! Sessions(...) abort
    if exists('b:_sessionsopt')
        let opt = b:_sessionsopt
    else
        let opt = copy(s:sessionsopt)
        call extend(opt, get(g:, 'sessionsopt', {}))
    endif
    for a in a:000
        call extend(opt, a)
    endfor

    let opt.sessiondir = s:fnamemodify(get(opt, 'sessiondir', get(g:, 'sessiondir', '')), ':p')
    if empty(opt.sessiondir) | throw 'no sessiondir' | endif

    let opt.since = 0 + opt.since
    let opt.max   = 0 + opt.max
    let opt.skip  = 0 + opt.skip

    if opt.since < 0 | let opt.since = 0 | endif
    if opt.max   < 0 | let opt.max   = 0 | endif
    if opt.skip  < 0 | let opt.skip  = 0 | endif

    let cachedir = opt.sessiondir..'/.cache/'
    call mkdir(cachedir, 'p')

    call s:movecrashedsessions(opt.sessiondir)

    """""""""""""""""""""""""""""""""""""""""""""""""""

    if exists('b:_sessionsopt')
        let init = get(opt, 'forceinit', 0)
        let opt.forceinit = 0
        setlocal modifiable
    else
        let init = 1
        enew
        setlocal buftype=nofile
        setlocal noswapfile
        setlocal nowrap
    endif

    let b:_sessionsopt = opt

    """""""""""""""""""""""""""""""""""""""""""""""""""

    if !empty(opt.since)
        let oldest = strftime('%s') - opt.since
        if oldest < 0 | let oldest = 0 | endif
    else
        let oldest = 0
    endif

    let sessions = []
    let dirs = ['/']
    while !empty(dirs)
        let dir = remove(dirs, 0)
        let _dir = opt.sessiondir..dir
        for item in readdir(_dir)
            if item[0] ==# '.' | continue | endif
            let session = _dir..item
            if isdirectory(session)
                call insert(dirs, dir..item..'/')
                continue
            endif
            if item !~ '\.vim$' | continue | endif
            let mtime = getftime(session)
            if mtime < oldest | continue | endif
            call add(sessions, [mtime, session, dir, item])
        endfor
    endwhile

    call sort(sessions, {a,b -> a[0]<b[0] ? 1 : a[0]>b[0] ? -1 : 0})

    """""""""""""""""""""""""""""""""""""""""""""""""""

    let lines = []
    let cnt = 0
    let total = len(sessions)

    let skip = opt.skip
    for [mtime, session, dir, item] in sessions
        if skip > 0
            let skip -= 1
            continue
        endif

        let cache = cachedir..substitute(session, '/', '%', 'g')
        let cacheversion = 3
        try
            if getftime(cache) < mtime
                throw ''
            else
                let data = json_decode(readfile(cache))
                if data.version != cacheversion | throw '' | endif
            endif
        catch
            " Extract Session Data {{{
            let stime = ''
            let tags = ''
            let jsonbufs = []
            let bufs = []
            let badds = []
            for line in readfile(session)
                "let m = matchlist(line, '\v^(%(badd|SessionTag)|" %(SessionTime|%(JSON )?Buffer)) (.*)')
                "if empty(m) | continue | endif
                
                let l = line[:12]

                if l ==# '" SessionTime'
                    let stime = line[14:]

                elseif l[:7] ==# '" Buffer'
                    call add(bufs, {'name':line[9:]})

                elseif l ==# '" JSON Buffer'
                    call add(jsonbufs, json_decode(line[14:]))

                elseif l[:3] ==# 'badd'
                    call add(badds, s:fnamemodify(expand(substitute(line[5:], '^\v\S+\s+', '', '')), ':p'))

                elseif l[:9] ==# 'SessionTag'
                    let tags = trim(line[11:])
                endif
            endfor
            " }}}

            let data = {
              \ 'version': cacheversion,
              \ 'stime': stime,
              \ 'tags': tags,
              \ 'jsonbufs': jsonbufs,
              \ 'bufs': bufs,
              \ 'badds': badds
              \ }
            call writefile(split(json_encode(data), "\n"), cache)
        endtry

   "            let stime = data.stime
   "            let tags = data.tags
   "            let jsonbufs = data.jsonbufs
   "            let bufs = data.bufs
   "            let badds = data.badds

        let bufs = []
        let seen = {}

        if !empty(data.jsonbufs)
            for buf in data.jsonbufs
                if empty(get(seen, buf.name))
                    let seen[buf.name] = 1
                    let buf._src = 'json'
                    if !SessionFilter(buf, opt)
                        call add(bufs, buf)
                    endif
                endif
            endfor
            " call filter(data.jsonbufs, {_,buf -> !SessionFilter(buf)})
            call sort(bufs, {
                \ a,b -> a.lastused<b.lastused ? 1 : a.lastused>b.lastused ? -1 : 0})
        else

            for buf in data.bufs
                if empty(get(seen, buf.name))
                    let seen[buf.name] = 1
                    let buf._src = 'v0'
                    if !SessionFilter(buf, opt)
                        call add(bufs, buf)
                    endif
                endif
            endfor

            call reverse(bufs)

            for badd in data.badds
                if empty(get(seen, badd))
                    let seen[badd] = 1
                    let buf = {'name':badd,'_src':'badd'}
                    if !SessionFilter(buf, opt)
                        call add(bufs, buf)
                    endif
                endif
            endfor

        endif

        if empty(bufs)
            let total -= 1
            continue
        endif

        if empty(data.stime)
            call add(lines, strftime('%F %T', mtime)..' '..dir[1:]..item)
        else
            call add(lines, data.stime..' '..dir[1:]..item)
        endif

        if !empty(data.tags)
            call add(lines, 'SessionTag '..data.tags)
        endif

        for buf in bufs
            call add(lines, s:sessionsbufformat(buf))
        endfor

        call add(lines, '')

        let cnt += 1
        if !empty(opt.max) && cnt >= opt.max
            break
        endif
    endfor

    """""""""""""""""""""""""""""""""""""""""""""""""""

"     py3 <<EOF
" def _():
"     from my.nvim import vim
"     import json
"     import os
"     import re
"     from time import time, strftime, localtime
" 
"     opt = vim.ev('opt')
" 
"     SESSIONDIR = opt['sessiondir']
"     SINCE = max((0, float(opt['since'])))
"     SKIP = max((0, int(opt['skip'])))
"     MAX = max((0, int(opt['max'])))
"     FILTER = opt.get('filter')
" 
"     if FILTER:
"         filter = re.compile(FILTER).search
"     else:
"         filter = lambda string: None
" 
" 
"     def pidstime(pid):
"         try:
"             return open(f"/proc/{pid}/stat").read().rpartition(")")[2].split(" ")[20]
"         except FileNotFoundError:
"             return 0
" 
"     path, dirs, files = next(os.walk(SESSIONDIR))
"     try: os.mkdir(f'{SESSIONDIR}/crash')
"     except FileExistsError: pass
"     for session in files:
"         split = session.split('.')
"         try:
"             prefix, time_, pid, ptime, suffix = split
"         except ValueError:
"             try:
"                 prefix, time_, pid, suffix = split
"                 ptime = -1
"             except ValueError:
"                 continue
"         if prefix != 'session' or suffix != 'vim':
"             continue
"         if pidstime(pid) == ptime:
"             continue
"         with open(f'{SESSIONDIR}/crash/{session}', 'wb') as f:
"             f.write(open(f'{path}/{session}','rb').read())
"         os.unlink(f'{path}/{session}')
" 
"     sessions = []
" 
"     if SINCE:
"         oldest = max((0, time() - SINCE))
"     else:
"         oldest = 0
" 
"     for _path, dirs, files in os.walk(SESSIONDIR):
"         path = _path.removeprefix(SESSIONDIR).removeprefix('/') or '.'
"         if path != '.' and os.path.basename(path).startswith('.'):
"             continue
"         for session in files:
"             if not session.startswith('session') or not session.endswith('.vim'):
"                 continue
"             full = f'{_path}/{session}'
"             mtime = os.stat(full).st_mtime
"             if mtime >= oldest:
"                 sessions.append((mtime, full, path, session))
" 
"     lines = []
"     count = 0
"     total = len(sessions)
" 
"     skip = SKIP
"     for mtime, full, path, session in sorted(sessions, reverse=True):
"         if skip > 0:
"             skip -= 1
"             continue
" 
"         # Init Session Data {{{
"         bufs = []
"         badds = []
"         tags = None
"         stime = None
"         # }}}
" 
"         # Extract Session Data {{{
"         with open(full, 'rb') as f:
" 
"             for line in f.read().split(b'\n'):
" 
"                 if line.startswith(b'" SessionTime '):
"                     stime = line[14:].strip().decode()
" 
"                 elif line.startswith(b'" Buffer '):
"                     buf = line[9:].decode()
"                     if filter(buf):
"                         continue
"                     if buf in bufs:
"                         continue
"                     bufs.append(buf)
" 
"                 elif line.startswith(b'" JSON Buffer '):
"                     buf = json.loads(line[15:].decode())
"                     if filter(buf):
"                         continue
"                     if buf in bufs:
"                         continue
"                     bufs.append(buf)
" 
"                 elif line.startswith(b'badd '):
"                     badd = line.partition(b' ')[2].partition(b' ')[2].decode()
"                     if filter(badd):
"                         continue
"                     if badd in badds:
"                         continue
"                     badds.append(badd)
" 
"                 elif line.startswith(b'SessionTag '):
"                     tags = line[11:].strip().decode()
"         # }}}
" 
"         if not bufs and not badds:
"             total -= 1
"             continue
" 
"         if not stime:
"             stime = strftime('%F %T', localtime(mtime))
" 
"         lines.append(f'{stime} {path}/{session}')
"         if tags:
"             lines.append(f'SessionTag {tags}')
"         for buf in bufs:
"             lines.append(f'  {buf}')
"         for badd in badds:
"             lines.append(f'   {badd}')
"         lines.append('')
" 
"         count += 1
"         if MAX and count >= MAX:
"             break
" 
"     since = f' since {SINCE/86400:.0f} days' if SINCE else ''
"     skipped = f' (skipped:{SKIP})' if SKIP else ''
"     max_ = f' (LIMITED)' if MAX and count == MAX else ''
"     vim.current.buffer[:] = (
"         f'SessionDir {SESSIONDIR}',
"         f'Sessions {count}/{total}{max_}{skipped}{since}',
"         '',
"         *lines)
" _()
" EOF

    """""""""""""""""""""""""""""""""""""""""""""""""""

    if !empty(opt.since) | let since = printf(' since %.0f days', opt.since/86400)
    else | let since = ''   | endif
    if !empty(opt.skip)  | let skipped = ' (skipped:'..opt.skip..')'
    else | let skipped = '' | endif
    if !empty(opt.max) && cnt != total  | let max_ = ' (LIMITED)'
    else | let max_ = ''    | endif

    silent! %d
    call setline(1, [
    \ 'SessionDir '..opt.sessiondir,
    \ 'Sessions '..cnt..'/'..total..max_..skipped..since,
    \ ''])
    call append(3, lines)

    normal ggjjj
    setlocal nomodified nomodifiable

    """""""""""""""""""""""""""""""""""""""""""""""""""

    if !empty(init)
        exe 'lcd '. fnameescape(opt.sessiondir)
        exe 'au BufWinEnter <buffer> lcd '. fnameescape(opt.sessiondir)

        command! -buffer -bar       -range                        SessionsOpen  call  s:opensession(<line1>, <line2>)
        command! -buffer -bar       -range                        SessionsEdit  call  s:editsession(<line1>, <line2>)
        command! -buffer      -bang -range -nargs=? -complete=dir SessionsCopy  call s:copysessions(<line1>, <line2>, <q-args>, !empty('<bang>'))
        command! -buffer      -bang -range -nargs=? -complete=dir SessionsMove  call s:movesessions(<line1>, <line2>, <q-args>, !empty('<bang>'))
        command! -buffer      -bang -range                        SessionsDel   call  s:delsessions(<line1>, <line2>, !empty('<bang>'))
        command! -buffer -bar -bang        -nargs=?               SessionsUndo  call s:undomovesessions((<q-args>=~?'\%[nounlink]'?1:0), !empty('<bang>'))

        nnoremap <silent> <buffer> <CR> :SessionsOpen<CR>
        " nnoremap <silent> <buffer>    i :SessionsEdit<CR>
        " nnoremap <silent> <buffer>    a :SessionsEdit<CR>
        " noremap  <silent> <buffer>    y :SessionsCopy<CR>
        " noremap  <silent> <buffer>    x :SessionsMove<CR>
        " noremap  <silent> <buffer>    d :SessionsDel<CR>
        " nnoremap <silent> <buffer>    u :SessionsUndo<CR>

        hi clear SessionsSummary
        hi clear SessionsName
        hi clear SessionsBuffer
        hi clear SessionsBufferFlag
        hi clear SessionsBufferFlags
        hi clear SessionsBadd

        hi link SessionsSummary       Todo
        hi link SessionsPrefix        Comment
        hi link SessionsPath          Directory
        hi link SessionsName          Comment
        hi link SessionsTag           Todo
        hi link SessionsBuffer        Identifier
        "hi link SessionsBufferFlag   Character
        "hi link SessionsBufferFlags  Comment
        hi link SessionsNoJson        Number
        hi link SessionsBufferFlags   Number
        hi link SessionsBufferLines   Comment
        hi link SessionsBadd          Constant

        syn clear
        syn on

        syn match Comment             /\v%(^\s\s\s)@<=\V[]/ containedin=SessionsBadd
        syn match SessionsBadd        /\v%(^\s\s\s)@<=\S.*/

        syn match SessionsNoJson      /\v%(^\s\s-\s+-\s+)@<=.*/ containedin=SessionsBuffer
        syn match SessionsBufferLines /\v%(^\s\s\S+\s+)@<=[0-9-]+/ containedin=SessionsBuffer
        syn match SessionsBufferFlags /\v%(^\s\s)@<=\S+/ containedin=SessionsBuffer
        "syn match SessionsBufferFlags /\v @<=\[\S*\|\d*\]$/ containedin=SessionsBuffer
        syn match SessionsBuffer      /\v%(^\s\s)@<=\S.*/

        syn match SessionsTag         /\v%(^SessionTag%(:|\s)\s*)@<=.*/
        syn match SessionsPrefix      /\v^SessionTag/

        syn match SessionsName        /^[0-9].*/
        syn match SessionsPath        /\v%(^[0-9]\S+\s+\S+\s+)@<=.*\/@=/ containedin=SessionsName

        syn match SessionsPrefix      /\v(%1l|%2l)^Sessions\S+/
        syn match SessionsSummary     /\v(%1l|%2l)%(^Sessions\S+\s+)@<=.*/

        setlocal foldenable foldmethod=indent shiftwidth=1 foldlevel=2
    endif
endfunction

function! s:sessionsbufformat(buf) abort
    let flags = (get(a:buf, 'hidden',    0) ? 'H' : get(a:buf, '_nwindows', 0) ? 'V' : '')
            \ ..(get(a:buf, 'loaded',    0) ? 'O' : '')
            \ ..(get(a:buf, 'listed',    0) ? 'L' : '')
            \ ..(get(a:buf, 'changed',   0) ? 'C' : '')
            \ ..{'':'','acwrite':'W','nowrite':'N'}[get(a:buf,'_buftype','')]
    if empty(flags) | let flags = '-' | endif
    let linecount = get(a:buf, 'linecount', '-')
    let space = repeat(' ', 7 - strlen(flags) - strlen(linecount))
    return '  '..flags..' '..space..linecount..' '..s:fnamemodify(a:buf.name, ':~')
    " return string([strlen(space), strlen(flags), strlen(linecount), '  ', flags, space, linecount, '  ', s:fnamemodify(a:buf.name, ':~')])
endfunction


function! s:findsessions(line, ...) abort
    if a:0
        let line2 = a:1
    else
        let line2 = a:line
    endif

    let sessiondir =  get(get(b:, '_sessionsopt', {}), 'sessiondir' ,'')
    if empty(sessiondir) | throw 'no b:_sessionopt.sessiondir' | endif

    let c = getcurpos()
    exe a:line
    normal! 0

    if search('^[0-9]', 'bcW')
        let sessions = [sessiondir..'/'..substitute(getline('.'), '\v^\S+\s+\S+\s+', '', '')]
    else
        let sessions = []
    endif

    while search('^[0-9]', 'W')
        if line('.') > line2
            break
        endif
        call add(sessions, sessiondir..'/'..substitute(getline('.'), '\v^\S+\s+\S+\s+', '', ''))
    endwhile

    call setpos('.', c)
    return sessions
endfunction


function! s:sessionswarning(msg) abort
    echohl WarningMsg
        for line in split(a:msg, "\n")
            echomsg line
        endfor
    echohl None
    return 1
endfunction

function! s:sessiongetdir() abort
    let sessiondir = get(get(b:, '_sessionsopt', {}), 'sessiondir' ,'')
    if empty(sessiondir) | throw 'no b:_sessionopt.sessiondir' | endif
    return sessiondir
endfunction


function! s:opensession(l1, l2) abort
    try
        let sessiondir = s:sessiongetdir()
        let paths = s:findsessions(a:l1, a:l2)
        if empty(paths) | return s:sessionswarning('no session') | endif
        let winid = win_getid()
        redir => out
            " TODO: silent! ?
            exe 'silent! source '. fnameescape(paths[0])
        redir END
        SessionFilter
        call win_gotoid(winid)
        redraw!
        exe 'silent! !kill -WINCH '. getpid()
        redraw!
        if !empty(out)
            return s:sessionswarning(out)
        endif
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
    echomsg 'Session loaded: '..paths[0]
endfunction

function! s:editsession(l1, l2) abort
    try
        let sessiondir = s:sessiongetdir()
        let paths = s:findsessions(a:l1, a:l2)
        if empty(paths) | return s:sessionswarning('no session') | endif
        exe 'edit '. fnameescape(paths[0])
        normal G
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction


function! SessionsCopy(paths, dest, ...) abort
    let unlink = get(a:000, 0, 0)
    let cont = get(a:000, 1, 0)
    call mkdir(a:dest, 'p')
    let errors = []
    let done = []
    for path in a:paths
        "echomsg [path, a:dest, unlink, cont]
        let newpath = a:dest..'/'..s:fnamemodify(path, ':t')
        try
            let lines = readfile(path, 'b')
            if empty(lines) | throw 'no lines' | endif
        catch
            call add(errors, [path, 'could not read session: '..v:exception])
            if empty(cont) | break | else | continue | endif
        endtry

        if empty(lines)
            call add(errors, [path, 'could not read session: '..path])
            if empty(cont) | break | else | continue | endif
        endif

        if writefile(lines, newpath, 'b') == -1
            call add(errors, [path, 'could not write destination: '..newpath])
            if empty(cont) | break | else | continue | endif
        endif


        call system('touch -r '..shellescape(path)..' '..shellescape(newpath))
        if !empty(v:shell_error)
            call add(errors, [path, 'could not update modification time on destination: '..newpath])
            if empty(cont) | break | else | continue | endif
        endif

        if !empty(unlink) && delete(path) == -1
            call add(errors, [path, 'could not unlink session: '..path])
            if empty(cont) | break | else | continue | endif
        endif

        call add(done, path)
    endfor
    return [done, errors]
endfunction

function! s:sessionscopyerrors(errors) abort
    if empty(a:errors) | return | endif
    let lines = []
    for [path, error] in a:errors
        call add(lines, path..': '..error)
    endfor
    return s:sessionswarning(join(lines, "\n"))
endfunction

function! s:_copysessions(l1, l2, dest, unlink, cont) abort
    if empty(a:dest)
        let dest = s:fnamemodify(input('destination: ', '', 'dir'), ':p')
    else
        let dest = s:fnamemodify(a:dest, ':p')
    endif
    let sessions = s:findsessions(a:l1, a:l2)
    if empty(sessions)
        return s:sessionswarning('no sessions')
    endif
    let [coppied, errors] = SessionsCopy(sessions, dest, a:unlink, a:cont)
    if !empty(a:unlink)
        let b:_sessionsundo = [coppied, s:fnamemodify(dest, ':p'), getcurpos()]
    endif
    call s:sessionscopyerrors(errors)
    call Sessions()
    exe a:l1
    if !empty(errors) | return 1 | endif
endfunction

function! s:copysessions(l1, l2, dest, cont) abort
    try
        call s:sessiongetdir()
        return s:_copysessions(a:l1, a:l2, a:dest, 0, a:cont)
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction

function! s:movesessions(l1, l2, dest, cont) abort
    try
        call s:sessiongetdir()
        return s:_copysessions(a:l1, a:l2, a:dest, 1, a:cont)
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction

function! s:delsessions(l1, l2, cont) abort
    try
        let sessiondir = s:sessiongetdir()
        let destination = sessiondir..'/.trash/'..strftime('%FT%H-%M-%S/')
        return s:_copysessions(a:l1, a:l2, destination, 1, a:cont)
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction


function! s:undomovesessions(nounlink, cont) abort
    try
        call s:sessiongetdir()
        if empty(get(b:, '_sessionsundo', 0))
            return s:sessionswarning('no sessions undo')
        endif
        let errors = []
        let [done, destination, pos] = b:_sessionsundo
        if empty(done)
            return s:sessionswarning('no sessions undo')
        endif
        let idx = 0
        while !empty(done)
            let moved = done[idx]
            let src = destination..'/'..s:fnamemodify(moved, ':t')
            let dest = s:fnamemodify(moved, ':h')
            let [_undone, _errors] = SessionsCopy([src], dest, empty(a:nounlink))
            call extend(errors, _errors)
            if !empty(_errors)
                if empty(a:cont) | break | endif
                let idx += 1
            else
                call remove(done, 0)
            endif
        endwhile
        call s:sessionscopyerrors(errors)
        call Sessions()
        call setpos('.', pos)
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction


""""""""""""""""""""""""""""""""""""""""

function! s:_Sessions(bang, line1, line2, args) abort
    try
        let m = matchlist(a:args, '\v^([^0-9.-]\S*)(\s+.*)?$')
        if empty(m)
            let args = split(a:args)
            let opt = {}
            if !empty(a:bang)                   | let opt.forceinit = 1                        | endif
            if len(args) > 0 && args[0] !=# '-' | let opt.since = str2float(args[0]) * 24*3600 | endif
            if len(args) > 1 && args[1] !=# '-' | let opt.max   = str2nr(args[1])              | endif
            if len(args) > 2 && args[2] !=# '-' | let opt.skip  = str2nr(args[2])              | endif
            call Sessions(opt)
        else
            let [c, args] = m[1:2]
            let c = tolower(c)
            let r = []
            if exists(':SessionsOpen')
                for cmd in s:_SessionsCommands
                    if stridx(cmd, c) != -1
                        call add(r, ['Sessions', cmd])
                    endif
                endfor
            endif
            for cmd in s:_SessionCommands
                if stridx(cmd, c) != -1
                    call add(r, ['Session', cmd])
                endif
            endfor
            if empty(r)
                throw 'no such sub-command: '..m[1]
            elseif len(r) > 1
                throw 'ambiguous sub-command: '..m[1]
            endif
            let [pre, cmd] = r[0]
            let cmd = toupper(cmd[0])..tolower(cmd[1:])
            exe (a:line1 == a:line2 ? '' : a:line1..','..a:line2)..pre..cmd..a:bang..' '..args
        endif
    catch
        echoerr v:throwpoint..': '..v:exception
        return 1
    endtry
endfunction

let s:_SessionsCommands = [
\ 'open',
\ 'edit',
\ 'copy',
\ 'move',
\ 'del',
\ 'undo'
\ ]

let s:_SessionCommands = [
\ 'filter',
\ 'tag',
\ 'new',
\ 'save',
\ ]

" function! s:_SessionsComplete(ArgLead, CmdLine, CursorPos) abort
"     let m = matchlist(a:CmdLine[:a:CursorPos-1], '\v^\S+\s+([^0-9.-]\S*)(\s*)(.*)')
"     if empty(m) | return s:_SessionsCommands | endif
"     if empty(m[2])
"         let r = []
"         let lower = tolower(m[1])
"         for cmd in s:_SessionsCommands
"             if stridx(cmd, lower) != -1
"                 call add(r, cmd)
"             endif
"         endfor
"         return r
"     endif
"     return getcompletion(m[3], 'dir')
" endfunction

function! s:_SessionsComplete(ArgLead, CmdLine, CursorPos) abort
    try
        let m = matchlist(a:CmdLine[:a:CursorPos-1], '\v^\S+\s+%(([^0-9.-]\S*)(\s*)(.*))?$')
        if empty(m)
            throw 'unexpected completion: '..string({'ArgLead':ArgLead, 'CmdLine':CmdLine, 'CursorPos':CursorPos})
            "return s:_SessionsCommands + s:_SessionCommands
        endif
        if empty(m[2])
            let r = []
            let lower = tolower(m[1])
            if exists(':SessionsOpen')
                for cmd in s:_SessionsCommands
                    if stridx(cmd, lower) == 0
                        call add(r, cmd)
                    endif
                endfor
            endif
            for cmd in s:_SessionCommands
                if stridx(cmd, lower) == 0
                    call add(r, cmd)
                endif
            endfor
            return r
        endif
        if index(s:_SessionsCommands, m[1]) != -1
            let pre = 'Sessions'
        elseif index(s:_SessionCommands, m[1]) != -1
            let pre = 'Session'
        else
            return []
        endif
        return getcompletion(pre..toupper(m[1][0])..m[1][1:]..' '..m[3], 'cmdline')
    catch
        throw 'ERROR: '..v:exception..'  (@'..v:throwpoint..')'
    endtry
endfunction

command! -bang -range -nargs=* -complete=customlist,s:_SessionsComplete Sessions call s:_Sessions('<bang>', <line1>, <line2>, <q-args>)
command! -bang -range -nargs=* -complete=customlist,s:_SessionsComplete Ss       call s:_Sessions('<bang>', <line1>, <line2>, <q-args>)
